import logging
from xml.etree import cElementTree as ET

def concat_xml_with_common_root(xml1, xml2):
	if xml1.tag != xml2.tag:
		raise Exception('XML nodes have different root tags')

	new_xml = ET.Element(xml1.tag)
	
	for el in list(xml1) + list(xml2):
		new_xml.append(el)

	return new_xml
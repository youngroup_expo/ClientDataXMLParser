import base64
import logging
import sys
import urllib2

from xml_helpers import concat_xml_with_common_root
from ConfigParser import ConfigParser

class XMLMiner:
    def __init__(self, config_name='DEFAULT'):
        config = ConfigParser()
        config.read('config.ini')
        if config == []:
            logging.error('Oops! There is no config.ini file')
            sys.exit()

        user = config.get(config_name, 'USER')
        password = config.get(config_name, 'PASSWORD')

        self.auth_string = base64.b64encode('%s:%s' % (user, password))
        self.url = config.get(config_name, 'URL')
        
    def get_xml(self, index, type_num):
        url = self.url + '?id=%s&type=%s' % (str(index), str(type_num))
        request = urllib2.Request(url)
        request.add_header("Authorization", "Basic %s" % self.auth_string)   
        result = urllib2.urlopen(request)
        
        if result.getcode() != 200:
            logging.warning('"%s" - status code %s' % (url, str(result.getcode())))
            return None
        
        return result.read()

    # def mine_xml(self, start_index, end_index):
    #     for index in range(start_index, end_index):


import sys
import logging
from xml.etree import cElementTree as ET

class XMLReducer:
    __ns = 'http://www.w3.org/2001/XMLSchema'

    def __init__(self, tag_list):
        self.tag_list = tag_list

    def reduce_string(self, xml):
        xml_root = ET.fromstring(xml)
        return self.__reduce_elem(xml_root)

    def reduce_file(self, xml_file):
        xml_root = ET.parse(xml_file).getroot()
        return self.__reduce_elem(xml_root)

    def __reduce_elem(self, xml_elem, reduced_parent=None):
        reduced_elem = None
        if xml_elem.tag in self.tag_list:
            reduced_elem = ET.Element(xml_elem.tag)
            reduced_elem.text = xml_elem.text
        elif reduced_parent is None:
            logging.warning('Root element is not in allowed tag list')
            return None

        if len(xml_elem) != 0:
            cur_reduced_parent = reduced_parent if reduced_elem is None else reduced_elem
            for child in list(xml_elem):
                reduced_child = self.__reduce_elem(child, cur_reduced_parent)
                if (reduced_child is not None):
                    cur_reduced_parent.append(reduced_child)

        return reduced_elem